#FROM centos:8.1.1911
FROM quay.io/kubespray/kubespray:v2.16.0

ENV KUBECONFIG=/root/.kube/admin.conf
ENV K8S_VERSION='1.20.7'
ENV HELM_VERSION='3.6.3'

RUN curl -o /usr/bin/kubectl -LO https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x /usr/bin/kubectl && \
    curl -o helm-v${HELM_VERSION}-linux-amd64.tar.gz -LO https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -xvf helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    cp linux-amd64/helm /usr/bin/helm && \
    rm -rf linux-amd64/ && \
    rm helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    chmod +x /usr/bin/helm && \
    mkdir /root/.kube

RUN apt-get update -y && \
    apt-get install vim -y && \
    rm -rf /var/lib/apt/lists/*

COPY k8s-lcm.sh /usr/bin/k8s-lcm.sh

RUN chmod +x /usr/bin/k8s-lcm.sh

ENTRYPOINT ["/bin/bash"]
