# k8s-lcm container

## Usage

0. start lcm container

```
docker run -itd -v --user=user $HOME/.ssh:/root/.ssh/ -v $HOME/inventory:/inventory --net=host --name=lc obald/k8s-lcm:latest
```

1. Prepare for install
```
docker exec lc k8s-lcm.sh setup mycluster 172.17.0.2 172.17.0.3 172.17.0.4
```

2. Go to inventory and modify your settings below as wanted

```
vim inventory/mycluster/group_vars/all/all.yml
vim inventory/mycluster/group_vars/k8s-cluster/k8s-cluster.yml
```

3. Do installation

```
docker exec lc k8s-lcm.sh install mycluster <user-name>
```

4. Do upgrade

Update k8s version and other component version to the one wanted in
the inventory config files like k8s-cluster.yml
Observe that kubespray only support upgrade in small steps.
git -b <version> checkout tag/<version>

```
vim inventory/mycluster/group_vars/k8s-cluster/k8s-cluster.yml
docker exec lc k8s-lcm.sh upgrade mycluster <user-name>
```

5. Do backup WIP!

```
docker exec lc k8s-lcm.sh backup mycluster <user-name> backup-path
```

6. Do restore WIP!

```
docker exec lc k8s-lcm.sh restore mycluster <user-name> backup-path
```
