#!/bin/bash

# MAIN
if [ "$#" -gt 0 ] ; then
    case "$1" in
        help)
            echo "k8s-lcm.sh [setup|install|upgrade|update|backup|restore]"
            echo
            echo "setup <clustername> <IPS> <user>"
            echo
            echo "install <clustername> <user>"
            echo
            echo "uninstall <clustername> <user>"
            echo
            echo "upgrade <clustername> <user>"
            echo
            echo "help"
            echo
            echo "docker run --itd -v <ssh-keys-path>:/root/.ssh/ -v $(pwd)/inventory:/inventory --net=host --name=k8s-lcm obald/k8s-lcm:latest"
            echo
            exit
        ;;
        uninstall|remove|cleanup)
            if [ "$#" -gt 2 ]; then
                clusterName="$2"
                nodeUser="$3"
                echo "uninstalling..."
                ansible-playbook -i /inventory/$clusterName/hosts.yaml --become --become-user=$nodeUser -u "$nodeUser" /kubespray/reset.yml
                echo "Uninstall Completed!"
            fi
            exit
            ;;
        setup)
            if [ "$#" -gt 2 ]; then
                clusterName="$2"
	        VALID_IP_ADDRESS="(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
                for ip in "$@"; do
                  if [[ $ip =~ $VALID_IP_ADDRESS ]]; then
                    IPS="$IPS $ip"
		    echo "$IPS"
                  fi
                done
	        echo "Setting up base inventory template $clusterName"
                cp -rfp /kubespray/inventory/sample /inventory/$clusterName
                declare -a IPS2=($IPS)
	        CONFIG_FILE="/inventory/$clusterName/hosts.yaml" python3 /kubespray/contrib/inventory_builder/inventory.py ${IPS2[@]}
                echo "Setup of base cluster inventory completed"
	        echo "Update /inventory/$clusterName/hosts.yaml to add or remove nodes"
	        echo "Update /inventory/$clusterName/group_vars/ configuration files if other than default settings wanted"
	        echo "After above two steps proceed with installation"
	        echo "install <clustername> <user>"
	    else
                echo "Not enough input"
	        echo "setup <clustername> <IPS>"
	    fi
	    exit
	    ;;
        install)
            if [ "$#" -gt 2 ]; then
                clusterName="$2"
                nodeUser="$3"
		if [ -d "/inventory/$clusterName" ]; then
                    ansible-playbook -i /inventory/$clusterName/hosts.yaml --become --become-user=root -u "$nodeUser" /kubespray/cluster.yml
	            mkdir -p "/inventory/$clusterName/.kube"
	            MASTER_IP=$(cat /inventory/$clusterName/hosts.yaml | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}' | grep "." | head -1)
                    ssh "$nodeUser@$MASTER_IP" 'sudo cat /etc/kubernetes/admin.conf' > /inventory/$clusterName/.kube/admin.conf
	            export KUBECONFIG="/inventory/$clusterName/.kube/admin.conf"
	            kubectl get pods --all-namespaces
	            echo "Install completed"
		else
		    echo "/inventory/$clusterName does not exist"
		fi
	    else
                echo "Not enough input"
                echo "install <clustername> <username>"
	    fi
            exit
            ;;
	upgrade)
            if [ "$#" -gt 2 ]; then
                clusterName="$2"
                nodeUser="$3"
		if [ -d "/inventory/$clusterName" ]; then
		    ansible-playbook -i /inventory/$clusterName/hosts.yaml --become --become-user=root -u "$nodeUser" /kubespray/upgrade-cluster.yml
	            MASTER_IP=$(cat /inventory/$clusterName/hosts.yaml | awk '{match($0,/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/); ip = substr($0,RSTART,RLENGTH); print ip}' | grep "." | head -1)
                    ssh "$nodeUser@$MASTER_IP" 'sudo cat /etc/kubernetes/admin.conf' > /inventory/$clusterName/.kube/admin.conf
	            export KUBECONFIG="/inventory/$clusterName/.kube/admin.conf"
	            kubectl get pods --all-namespaces
	            echo "Upgrade completed"
		else
		    echo "/inventory/$clusterName does not exist"
		fi
	    else
                echo "Not enough input"
                echo "upgrade <clustername> <username>"
	    fi
            exit
            ;;
        *)
            echo "Usage: docker run --itd -v <ssh-keys-path>:/root/.ssh/ -v $(pwd)/inventory:/inventory --net=host --name=k8s-lcm obald/k8s-lcm:latest"
            echo
            echo "k8s-lcm.sh [setup|install|upgrade|update|backup|restore]"
            echo
            echo "setup <clustername> <IPS> <user>"
            echo
            echo "install <clustername> <user>"
            echo
            echo "uninstall <clustername> <user>"
            echo
            echo "upgrade <clustername> <user>"
            echo
            echo "help"
            echo
            exit
            ;;
    esac
fi
