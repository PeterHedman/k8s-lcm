_image = 'obald/k8s-lcm'
_version = '1.16.0'
_time = $(shell date +"%Y%m%d_%H%M%S")

.PHONY: build
build:
	docker build --network=host --tag $(_image):$(_version) .
	docker tag $(_image):$(_version) $(_image):latest

build-buildah:
	buildah bud --network=host --no-cache --tag $(_image):$(_version) .
	docker tag $(_image):$(_version) $(_image):latest

.PHONY: clean
clean:
	docker rmi $(_image):$(_version) $(_image):latest

.PHONY: start-test
start-test:
	docker run --rm -it --mount type=bind,source="$(HOME)"/inventory/,dst=/inventory --mount type=bind,source="${HOME}"/.ssh/id_rsa,dst=/root/.ssh/id_rsa --name testing-k8s-lcm $(_image):$(_version)

.PHONY: start-test-selinux
start-test-selinux:
	docker run --rm -it --mount type=bind,source="$(HOME)"/inventory/,dst=/inventory,rw,Z --mount type=bind,source="${HOME}"/.ssh/id_rsa,dst=/root/.ssh/id_rsa:ro,Z --name test2-k8s-lcm $(_image):$(_version)

.PHONY: stop_test
stop-test:
	docker stop testing-k8s-lcm
	docker rm testing-k8s-lcm
	@echo '$(PWD)/inventory needs to be removed'

.PHONY: push
push:
	docker tag $(_image):$(_version) $(_image):$(_version)-$(_time)
	docker push $(_image):$(_version)
	docker push $(_image):latest
	docker push $(_image):$(_version)-$(_time)

.PHONY: help
help:
	@echo 'Usage'
	@echo '  make build      : Builds $(_image) container'
	@echo '  make clean      : Remove built $(_image) container'
	@echo '  make start-test : Starts a test'
	@echo '  make stop-test  : Stops and removes test'
	@echo '  make push       : Push built docker images'
	@echo '  make help       : Prints this message'
